export type Product = {
  id: number;
  title: string;
  price: number;
  stateId: number;
  categoryId: number;
  stock: boolean;
  picture: string;
  description: string;
};

export type State = {
  id: number;
  name: string;
  tax: number;
};

export type Category = {
  id: number;
  name: string;
};
