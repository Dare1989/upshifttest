import { Category, Product, State } from "./interfaces";
import { API_URL } from "./constants";

export const getProducts = async (): Promise<Product[]> =>
  await (await fetch(`${API_URL}/products`)).json();

export const postProduct = async (product: Product): Promise<Product[]> =>
  await (
    await fetch(`${API_URL}/products`, {
      method: "post",
      body: JSON.stringify(product),
    })
  ).json();

export const getProduct = async (id: number): Promise<Product> =>
  await (await fetch(`${API_URL}/products/${id}`)).json();

export const getCategories = async (): Promise<Category[]> =>
  await (await fetch(`${API_URL}/categories`)).json();

export const getState = async (): Promise<State[]> =>
  await (await fetch(`${API_URL}/states`)).json();
