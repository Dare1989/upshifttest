import { BrowserRouter, Route, Routes, Link } from "react-router-dom";
import "./App.css";
import Products from "./pages/Products";
import ProductPage from "./pages/Product";

const App = () => {
  return (
    <div className="App">
      <BrowserRouter>
        <br />
        <div className="navbar">
          <div>
            <Link className="allProducts" to="/">
              All Products
            </Link>
          </div>
        </div>
        <div className="main">
          <Routes>
            <Route path="/" element={<Products />} />
            <Route path="/products/:id" element={<ProductPage />} />
          </Routes>
        </div>
      </BrowserRouter>
    </div>
  );
};

export default App;
