import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Product } from "../utils/interfaces";
import { getProduct } from "../utils/api";

const ProductPage = () => {
  const [product, setProduct] = useState<Product>({
    id: 0,
    title: "",
    price: 0,
    stateId: 0,
    categoryId: 0,
    stock: true,
    picture: "",
    description: "",
  });

  const params = useParams();
  const id = parseInt(params.id || "");

  useEffect(() => {
    getProduct(id).then((r) => setProduct(r));
  }, []);

  return (
    <div className="card">
      <img src={product.picture} alt={product.title} />
      <div className="cardInner">
        <h3>{product.title}</h3>
        <p>{product.description}</p>
        <h3>{product.price} $</h3>
      </div>
    </div>
  );
};

export default ProductPage;
