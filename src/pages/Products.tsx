import React, { useEffect, useState } from "react";
import Card from "../components/Card/Card";
import { getProducts } from "../utils/api";
import { Product } from "../utils/interfaces";
import "../App.css";
import Modal from "../components/Modal/Modal";

const Products = () => {
  const [showProductModal, setShowProductModal] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(true);
  const [products, setProducts] = useState<Product[]>([]);
  const [filteredProducts, setFilteredProducts] = useState<Product[]>([]);
  useEffect(()=>{
    setFilteredProducts(products);
    return () => {};
  }, [products]);
  useEffect(() => {
    getProducts().then((products) => {
      setProducts(products);
      setLoading(false);
    });
    return () => {};
  }, []);

  const handleFilterSelect = (e: any) => {
    if (e.target.value === "All Cards") {
      setFilteredProducts(products);
    } else {
      const item = products?.filter((obj) => obj.title === e.target.value);
      setFilteredProducts(item);
    }
  };

  const handlePriceSelect = (e: any) => {
    if (e.target.value === "Filter by Price") {
      setFilteredProducts(products);
    } else {
      const item = products?.filter(
        (obj) => obj.price === parseInt(e.target.value)
      );
      setFilteredProducts(item);
    }
  };

  if (loading) return <div>loading ...</div>;

  return (
    <div>
      <div className="productList">
        <div>
          <select
            className="custom-select"
            onChange={(e) => handlePriceSelect(e)}
          >
            <option>Filter by Price</option>
            {products?.map((el, i) => {
              return (
                <option key={i} value={el.price}>
                  {el.price} %
                </option>
              );
            })}
          </select>
          <select
            className="custom-select"
            onChange={(e) => handleFilterSelect(e)}
          >
            <option>All Cards</option>
            {products?.map((el, i) => {
              return (
                <option key={i} value={el.title}>
                  {el.title}
                </option>
              );
            })}
          </select>
        </div>
        <div className="modalAdd">
          <button
            className="button"
            onClick={() => {
              setShowProductModal(true);
            }}
          >
            Add
          </button>
        </div>
      </div>
      <div className="main">
        {filteredProducts?.map((item) => (
          <Card key={item.id} item={item} />
        ))}
      </div>
      {showProductModal ? (
        <Modal setShowProductModal={setShowProductModal} />
      ) : null}
    </div>
  );
};

export default Products;
