import { useState } from "react";
import { useQuery } from "react-query";
import { getCategories, getState, postProduct } from "../../utils/api";
import { Category, State, Product } from "../../utils/interfaces";
import "./Modal.css";

const Modal = ({ setShowProductModal }: any) => {
  const [product, setProduct] = useState<Product>({
    id: 0,
    title: "",
    price: 0,
    stateId: 0,
    categoryId: 0,
    stock: true,
    picture: "",
    description: "",
  });
  const [state, setState] = useState<State>({
    id: 0,
    name: "",
    tax: 0,
  });
  const { data: categories } = useQuery<Category[]>("category", getCategories);
  const { data: states } = useQuery<State[]>("state", getState);

  const handleFormValidation = () => {
    const { title, price, picture, stateId, categoryId } = product;

    if (title.length <= 3) {
      alert("Title is required.");
      return;
    }

    if (price <= 4) {
      alert("Price must be higher than 4");
      return;
    }

    if (stateId < 1) {
      alert("State is required field.");
      return;
    }

    if (state.tax > 0.25) {
      if (price < 7) {
        alert("The price must be larger than 7.");
        return;
      }
    }

    if (categoryId < 1) {
      alert("Category is required field.");
      return;
    }

    if (!picture.startsWith("http://") && !picture.startsWith("https://")) {
      alert("Picture is required and must be a valid url.");
      return;
    }

    postProduct(product);
    alert("Product added");
    window.location.reload();
  };
  const updateProduct = (e: any) => {
    setProduct({
      ...product,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <>
      <div className="overlay">
        <div className="popup">
          <div className="popupBtn">
            <span
              className="btnClose"
              onClick={() => {
                setShowProductModal(false);
              }}
            >
              X
            </span>
          </div>
          <div className="title">
            <h2>Insert Product</h2>
          </div>
          <div className="content">
            <select name="categoryId" onChange={updateProduct}>
              <option>Category</option>
              {categories?.map((category) => (
                <option key={category.id} value={category.id}>
                  {category.name}
                </option>
              ))}
            </select>
            <select name="stateId" onChange={updateProduct}>
              <option>State</option>
              {states?.map((state) => (
                <option
                  key={state.id}
                  value={state.id}
                  onClick={() => {
                    setState(state);
                  }}
                >
                  {state.name}
                </option>
              ))}
            </select>
          </div>
          <div className="mainContent">
            <div>
              <input
                placeholder="title"
                value={product.title}
                name="title"
                onChange={updateProduct}
              />

              <input
                placeholder="price"
                value={product.price}
                name="price"
                onChange={updateProduct}
              />
            </div>
            <input
              placeholder="Img url"
              value={product.picture}
              required
              name="picture"
              onChange={updateProduct}
            />
            <input
              placeholder="Description"
              value={product.description}
              name="description"
              onChange={updateProduct}
            />
          </div>
          <div className="btnBottom">
            <button onClick={handleFormValidation} className="close">
              Add Item
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Modal;
