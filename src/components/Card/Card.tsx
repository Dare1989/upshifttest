import { Product } from "../../utils/interfaces";
import "./Card.css";
import { Link } from "react-router-dom";

type Props = {
  item: Product;
};

const Card: React.FC<Props> = ({ item }) => (
  <div className="card">
    <Link className="linkTag" to={`/products/${item.id}`}>
      <img src={item.picture} alt={item.title} />
      <div className="cardInner">
        <h3>{item.title}</h3>
        <p>{item.description}</p>
        <h3>{item.price} $</h3>
      </div>
    </Link>
  </div>
);

export default Card;
